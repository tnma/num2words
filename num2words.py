from flask import Flask, render_template
import controller

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html', title='num2words', message='welcome to num2words')


@app.route('/num/<number>')
def num(number):
    return controller.convert_num_to_en(int(number.replace(',', '')))


@app.route('/currency/<amount>')
def currency(amount):
    val = amount.replace(',', '').split('.')
    dollars = val[0]
    cents = 0 if len(val) < 2 else val[1]
    return controller.convert_currency_to_en(int(dollars), int(cents))


if __name__ == '__main__':
    app.run()
