import flask


def convert_num_to_en(num, more_val=0):
    ones = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
    teens = ['', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
    tens = ['', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']
    # should support up to 999,999,999,999,999,999,999,999,999,999
    more = ['', 'hundred', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion']
    length = len(str(num))
    more_words = ' ' + more[more_val]
    if length == 1:
        return ones[num] + more_words
    elif 1 < length < 4:
        tens_words = (tens[int(str(num)[length - 2])] + ('-' + ones[int(str(num)[length - 1])] if
                                                         int(str(num)[length - 1]) != 0 else ''))
        if 10 < num % 100 < 20:
            tens_words = teens[(num % 100) - 10]
        return (ones[int(str(num)[0])] + ' ' + more[1] if length == 3 else '') + ' ' + tens_words + more_words
    else:
        # recursive calls to handle numbers bigger than 999 and append the proper suffix
        if more_val == 0:
            return convert_num_to_en(num / 1000, 2) + ' ' + (convert_num_to_en(num % 1000) if num % 1000 != 0 else '')
        else:
            return convert_num_to_en(num / 1000, more_val + 1) + ' ' + (
                convert_num_to_en(num % 1000, more_val) if num % 1000 != 0 else '')


def convert_currency_to_en(dollars, cents):
    # reuse convert_num_to_en and append proper suffixes for dollars and cents
    dollars = convert_num_to_en(dollars)
    # return HTTP 500 if cents is out of bounds
    if cents > 100:
        flask.abort(500)
    cents = convert_num_to_en(cents)
    return dollars + ' dollars and ' + cents + ' cents'
